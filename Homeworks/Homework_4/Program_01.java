class Program_01 {
    public static void main(String[] args) {
        int[] array = {1, 3, 9, 6, 8, 6, 7, 3, 4};
        System.out.print(parse(array));
    }
    public static int parse(int[] array) {
        int result = 0;
        for (int i = 0; i < array.length; i++) {
            result = result * 10 + array[i];
        }
        return result;
    }
}
