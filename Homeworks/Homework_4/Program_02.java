import java.util.Scanner;

class Program_02 {
	public static void main (String[] args) {
		System.out.println(result(inputArray()));
	}

	public static int[] inputArray() {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Введи количество чисел");

		int arraylength = scanner.nextInt();
		int[] numbers = new int[arraylength];

		System.out.println("Введи числа от -1000 до 1000");

		for(int i = 0; i < arraylength; i++) {
			numbers[i] = scanner.nextInt();
			if(numbers[i] > 1000 || numbers[i] < -1000) {					//проверяем вход в диапазон, если нет, то откатываем счётчик, число перезаписывается
				System.out.println("Число вне диапазона, введи другое");
				i--;
			}
		}
		return numbers;
	}

	public static int result (int[] array) {
		int number = 0;
		int count = 0;
		int max = 0;

		for (int i = 0; i < array.length; i++) {
			for(int j = 0; j < array.length; j++) {
				if(array[i] == array[j]) {
					count++;
				}
			}
			if(count >= max) {
				number = array[i];
				max = count;
			} 
			count = 0;
		}
		//if(max < 1) {
		//	System.out.println("Совпадающих чисел нет");
		//}
		return number;
	}
}
