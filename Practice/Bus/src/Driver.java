public class Driver {
    private String name;
    private int experience;
    private Bus bus;

    public Driver(String name, int experience) {
        this.name = name;
        this.experience = experience;
    }

    public String getName() {
        return name;
    }

    public int getExperience() {
        return experience;
    }

    public void setBus(Bus bus) {
        if (this.bus == null) {
            this.bus = bus;
            this.bus.setDriver(this);
        }
        System.out.println("У водителя уже есть автобус");
    }
    public void drive(){
        if (this.bus != null) {
            this.bus.go();
        }
        else {
            System.err.println("У этого водителя нет автобуса");
        }
    }
}
