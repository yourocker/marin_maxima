public class TV {
    private final int MAX_CHANNELS_ON = 1;

    private String model;
    public Channel[] channels;
    private int channelsCount;
    private RemoteController remoteController;

    public TV(String model) {
        this.model = model;
        this.channels = new Channel[MAX_CHANNELS_ON];
        this.channelsCount = 0;
    }

    //turn channel on
    public boolean getChannel(Channel channel) {
        if (channelsCount < MAX_CHANNELS_ON) {
            channels[channelsCount] = channel;
            System.out.println("включен канал " + channel.getChName());
            channelsCount++;
            return true;
        } else {
            System.out.println("переключено на канал номер " + channel.getChNumber() + " "
                    + channel.getChName());
            channelsCount++;
            return false;
        }
    }

    public String getModel() {
        return model;
    }
}
