public class Main {

    public static void main(String[] args) {
        Driver anatoly = new Driver("Anatoly", 10);
        Driver kostyan = new Driver("Kostyan", 5);

        Bus bus55 = new Bus(52, "Ford");
        Bus bus22 = new Bus(22, "ПАЗ");

        Passenger vasya = new Passenger("Vasya");
        Passenger petya = new Passenger("Petya");
        Passenger leisan = new Passenger("Leisan");
        Passenger yura = new Passenger("Yura");
        Passenger maxim = new Passenger("Maxim");
        Passenger denis = new Passenger("Denis");

        vasya.goToBus(bus55);
        petya.goToBus(bus55);
        leisan.goToBus(bus55);
        yura.goToBus(bus55);

        maxim.goToBus(bus22);
        denis.goToBus(bus22);

        vasya.goToBus(bus22);

        anatoly.setBus(bus22);
        kostyan.setBus(bus55);

        anatoly.drive();
        kostyan.drive();

    //    System.out.println(vasya.isInBus());
    //    System.out.println(petya.isInBus());
    //    System.out.println(leisan.isInBus());
    //    System.out.println(yura.isInBus());
     }
}
