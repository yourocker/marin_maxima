public class Main {

    public static void main(String[] args) {

        TV lg = new TV("LG");

        RemoteController rc = new RemoteController();

        Channel discovery = new Channel("Discovery", 1);
        Channel rbc = new Channel("RBC", 2);
        Channel nationalGeo = new Channel("National Geographic", 3);
        Channel cartoonNet = new Channel("Cartoon Network", 4);
        Channel paramoutComedy = new Channel("Paramout Comedy", 5);

        Program friends = new Program("Friends");
        Program southpark = new Program("South Park");
        Program simpsons = new Program("The Simpsons");
        Program modernFamily = new Program("Modern Family");
        Program powerRangers = new Program("Power Rangers");
        Program transformers = new Program("Transformers");
        Program news = new Program("News");
        Program politics = new Program("Politics");
        Program mythBusters = new Program("Myth Busters");
        Program wildTuna = new Program("Wild Tuna");

        rbc.goToTV(lg);
        nationalGeo.goToTV(lg);
        cartoonNet.goToTV(lg);
        paramoutComedy.goToTV(lg);
        discovery.goToTV(lg);

        friends.goToChannel(rbc);
        politics.goToChannel(rbc);

    }
}
