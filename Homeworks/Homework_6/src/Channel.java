public class Channel {
    private final int MAX_PROGRAMS_COUNT = 1;
    public String chName;
    public int chNumber;
    private boolean onTV;
    private TV tv;
    private int programCount = 0;
    private Program[] programs;

    public Channel(String chName, int chNumber) {
        this.chName = chName;
        this.chNumber = chNumber;
        this.onTV = false;
        this.programs = new Program[MAX_PROGRAMS_COUNT];
    }

    //включается только первый канал, дебажил, мозга не хватает понять пока как исправить.
    public void goToTV(TV tv) {
        if (!onTV) {
            if (tv.getChannel(this)) {
                onTV = true;
                this.tv = tv;
            }
        } else {
            System.err.println("канал " + getChName() + " под номером " + getChNumber()
                    + " уже включен на телевизоре " + this.tv.getModel());
            onTV = false;
        }
    }

    //передачи валятся только на выбранный канал, хочется сделать чтобы падали на текущий
    public boolean getProgram(Program program) {
        if (programCount < 1) {
            programs[programCount] = program;
            System.out.println("идёт программа " + program.getPrName() + " по каналу " + getChName());
            programCount++;
            return true;
        } else {
            System.out.println("программа сменилась на " + program.getPrName()); //не работает переключение
            programCount++;
            return false;
        }
    }

    public String getChName() {
        return chName;
    }

    public int getChNumber() {
        return chNumber;
    }

    public boolean isOnTV() {
        return onTV;
    }
}
