import java.util.Scanner;

class Program
{
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		int	i = 0;
		int count = 0;
		int digitsSum = 0;
		
		while (i < 10) {
			int currentNumber = scanner.nextInt();

			digitsSum = 0;

			while(currentNumber != 0) {
				digitsSum += currentNumber % 10;		
				currentNumber = currentNumber / 10;		
			}
			if (digitsSum < 18) {
				count++;								
			}
			i++;
		}
	System.out.println(count);		
	}
}
