public class Program {
    public String prName;
    private boolean onChannel;
    private Channel channel;

    public Program(String prName) {
        this.prName = prName;
        this.onChannel = false;
    }

    public void goToChannel(Channel channel) {
        if (!onChannel) {
            if (channel.getProgram(this)) {
                onChannel = true;
                this.channel = channel;
            }
        } else {
            System.err.println("программа " + getPrName() + " уже идёт по каналу" + getChannel());
            onChannel = false;
        }
    }

    public Channel getChannel() {
        return channel;
    }

    public String getPrName() {
        return prName;
    }

    public boolean isOnChannel() {
        return onChannel;
    }
}
