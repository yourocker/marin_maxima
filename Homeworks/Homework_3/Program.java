import java.util.Scanner;

class Program
{
	public static void main (String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		int number = scanner.nextInt();
		while(number != -1)								//вводим стоп-число))
		{
			if(isEven(number))							//если чётное
			{
				System.out.println(minDigit(number));
			}
			if(!isEven(number))							//если нечётное (!)
			{
				System.out.println(getDigitsAverage(number));
			}
		number = scanner.nextInt();						//ввод с консоли
		}
	}

	public static double getDigitsAverage(int number)	//вычисление среднеарифметического
	{
		double count = 0;
		double result = 0;
		while(number != 0)
		{
			result += number % 10;
			number = number / 10;
			count++;
		}
		return result/count;
	}

	public static int minDigit(int number)				//поиск минимальной цифры числа
	{
		int min = 9;
		while(number > 0)
		{
			int temp = number % 10;
			if(temp < min)
			{
				min = temp;
			}
			number /= 10;
		}
		return min;
	}

	public static boolean isEven(int number)			//проверка на чётность
	{
		if(number % 2 == 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}

